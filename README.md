# Quick and dirty prototype for demo visu

## Requirements

```
opam install graphics
```

## Build

```
make
```

## Usage

```
./gen.exe 100 img/logo.ppm img/ocaml.ppm img/rocket.ppm
```

produces something like:

```
400 400 25 4 /tmp/genfb76d1.0 /tmp/gen07306d.1 /tmp/gendce5e1.2 /tmp/gen5a99bd.3 /tmp/gen926c10.4 /tmp/gen020d93.5 /tmp/genf54661.6 /tmp/gene20daf.7 /tmp/gen4dceba.8 /tmp/gen88ead5.9 /tmp/gen3fc7ca.10 /tmp/genb64251.11 /tmp/gen4d0d12.12 /tmp/gen24b939.13 /tmp/gen99b7c9.14 /tmp/genfb4f19.15 /tmp/genb820aa.16 /tmp/gen40e80a.17 /tmp/gene9ca55.18 /tmp/genff33f5.19 /tmp/gen20ea92.20 /tmp/gene2d7fd.21 /tmp/gen38c792.22 /tmp/gen1690d2.23 /tmp/gend7510f.24 /tmp/gen720851.25 /tmp/gen4ed750.26 /tmp/gen3a0feb.27 /tmp/genb3d1c7.28 /tmp/gen752bc2.29 /tmp/genbce588.30 /tmp/genfc19f0.31 /tmp/gen68f5c3.32 /tmp/gen631905.33 /tmp/gen1c9906.34 /tmp/gen3d997f.35 /tmp/gen9c6dae.36 /tmp/genbf7304.37 /tmp/genb8c4e8.38 /tmp/gene81b76.39 /tmp/gen1f0b92.40 /tmp/gen65b901.41 /tmp/genf9e653.42 /tmp/genc52d62.43 /tmp/gen205af5.44 /tmp/genafa2e8.45 /tmp/gen7824d1.46 /tmp/gen721e81.47 /tmp/genba5878.48 /tmp/gen54e686.49 /tmp/gen37e2f6.50 /tmp/gen28452e.51 /tmp/genc89213.52 /tmp/gen6140f0.53 /tmp/gena4f721.54 /tmp/gend2c7dc.55 /tmp/gen7938bb.56 /tmp/gen2bbd46.57 /tmp/gen94a99a.58 /tmp/gen463dcf.59 /tmp/gen773946.60 /tmp/genb22c04.61 /tmp/gende279f.62 /tmp/genb6c475.63 /tmp/genec03dd.64 /tmp/gen5c60d0.65 /tmp/genf3dbd1.66 /tmp/gene97ac1.67 /tmp/gene42c42.68 /tmp/gen527cc6.69 /tmp/gene36a82.70 /tmp/gend72c36.71 /tmp/gena6a109.72 /tmp/gene3b0a4.73 /tmp/genc46149.74 /tmp/gen414582.75 /tmp/gen927acb.76 /tmp/genb0ec6d.77 /tmp/gen88af59.78 /tmp/gen91a993.79 /tmp/genc6eed7.80 /tmp/gen86ba53.81 /tmp/gen625515.82 /tmp/gen7c63eb.83 /tmp/gend66cc6.84 /tmp/gen61dcc0.85 /tmp/gencee1da.86 /tmp/gene22e8e.87 /tmp/gen8ed662.88 /tmp/genb142b9.89 /tmp/gen83d737.90 /tmp/gen13a6f2.91 /tmp/gen719f13.92 /tmp/gen96ec92.93 /tmp/gen9edfe2.94 /tmp/gen0b0f6b.95 /tmp/genc219d8.96 /tmp/genedc22e.97 /tmp/genaea827.98 /tmp/gen04873c.99
```

that can be used as argument to `./visu.exe`:

```
./visu.exe 400 400 25 4 /tmp/genfb76d1.0 /tmp/gen07306d.1 /tmp/gendce5e1.2 /tmp/gen5a99bd.3 /tmp/gen926c10.4 /tmp/gen020d93.5 /tmp/genf54661.6 /tmp/gene20daf.7 /tmp/gen4dceba.8 /tmp/gen88ead5.9 /tmp/gen3fc7ca.10 /tmp/genb64251.11 /tmp/gen4d0d12.12 /tmp/gen24b939.13 /tmp/gen99b7c9.14 /tmp/genfb4f19.15 /tmp/genb820aa.16 /tmp/gen40e80a.17 /tmp/gene9ca55.18 /tmp/genff33f5.19 /tmp/gen20ea92.20 /tmp/gene2d7fd.21 /tmp/gen38c792.22 /tmp/gen1690d2.23 /tmp/gend7510f.24 /tmp/gen720851.25 /tmp/gen4ed750.26 /tmp/gen3a0feb.27 /tmp/genb3d1c7.28 /tmp/gen752bc2.29 /tmp/genbce588.30 /tmp/genfc19f0.31 /tmp/gen68f5c3.32 /tmp/gen631905.33 /tmp/gen1c9906.34 /tmp/gen3d997f.35 /tmp/gen9c6dae.36 /tmp/genbf7304.37 /tmp/genb8c4e8.38 /tmp/gene81b76.39 /tmp/gen1f0b92.40 /tmp/gen65b901.41 /tmp/genf9e653.42 /tmp/genc52d62.43 /tmp/gen205af5.44 /tmp/genafa2e8.45 /tmp/gen7824d1.46 /tmp/gen721e81.47 /tmp/genba5878.48 /tmp/gen54e686.49 /tmp/gen37e2f6.50 /tmp/gen28452e.51 /tmp/genc89213.52 /tmp/gen6140f0.53 /tmp/gena4f721.54 /tmp/gend2c7dc.55 /tmp/gen7938bb.56 /tmp/gen2bbd46.57 /tmp/gen94a99a.58 /tmp/gen463dcf.59 /tmp/gen773946.60 /tmp/genb22c04.61 /tmp/gende279f.62 /tmp/genb6c475.63 /tmp/genec03dd.64 /tmp/gen5c60d0.65 /tmp/genf3dbd1.66 /tmp/gene97ac1.67 /tmp/gene42c42.68 /tmp/gen527cc6.69 /tmp/gene36a82.70 /tmp/gend72c36.71 /tmp/gena6a109.72 /tmp/gene3b0a4.73 /tmp/genc46149.74 /tmp/gen414582.75 /tmp/gen927acb.76 /tmp/genb0ec6d.77 /tmp/gen88af59.78 /tmp/gen91a993.79 /tmp/genc6eed7.80 /tmp/gen86ba53.81 /tmp/gen625515.82 /tmp/gen7c63eb.83 /tmp/gend66cc6.84 /tmp/gen61dcc0.85 /tmp/gencee1da.86 /tmp/gene22e8e.87 /tmp/gen8ed662.88 /tmp/genb142b9.89 /tmp/gen83d737.90 /tmp/gen13a6f2.91 /tmp/gen719f13.92 /tmp/gen96ec92.93 /tmp/gen9edfe2.94 /tmp/gen0b0f6b.95 /tmp/genc219d8.96 /tmp/genedc22e.97 /tmp/genaea827.98 /tmp/gen04873c.99
```

`./visu.exe` should show the image reconstructed by reading the
hundred of files generated by `./gen.exe`.

`./visu.exe` also produces a bunch of `out-xxxx.ppm` files that can be
turned into a video stream like this:

```
ffmpeg -re -stream_loop -1 -framerate 10 -i out-%4d.ppm -preset ultrafast -vcodec libx264 -tune zerolatency -b:v 500k -f mpegts udp://127.0.0.1:1234
```

After some bufferization time, the following command display the resulting video stream:
```
ffplay udp://127.0.0.1:1234
```

NOTE: The images have been generated using the following command:

```
convert -resize 400x400\! -compress none [infile] [outfile]
```

(`convert` being an imagemagick utilities.)

## TODO

- use more efficient image format for the image sequence (PPM P5 is really the worst :-)
  e.g., PPM in binary P6 or PNG
